# Usa o sistema operacional Debian como base
FROM debian:8.4

# Adiciona uma vari�vel para facilitar a altera��o de vers�es futuras
ENV JAVA_VERSION 8u91
# E adiciona a vari�vel de ambiente JAVA_HOME
ENV JAVA_HOME /usr/lib/jvm/jdk1.8.0_91/

# Copia o arquivo do Java para dentro do container
COPY jdk-JAVA_VERSION-linux-x64.tar.gz /usr/lib/jvm/

# Acessa a pasta onde o Java ser� instalado:
WORKDIR /usr/lib/jvm

# Descompacta o Java e remove o arquivo zipado:
RUN tar -zxvf jdk-$JAVA_VERSION-linux-x64.tar.gz && \
    rm jdk-JAVA_VERSION-linux-x64.tar.gz

# Adiciona o Java no PATH
ENV PATH "$PATH":/	{JAVA_HOME}/bin:.: