<%@page import="java.lang.String"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<head>
<title>Lyncas Cadastro</title>
<meta charset="UTF-8">
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id"
	content="939508598651-b77pcu0efq78mcgrd592t9jjh6s0sskq.apps.googleusercontent.com">
<script src="https://apis.google.com/js/platform.js" async defer></script>
<link rel="stylesheet" type="text/css"
	href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="css/Principal.css" />
<link rel="SHORTCUT ICON" href="img/favicon.ico" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<!-- Static navbar -->
	<nav class="navbar navbar-default">
		<div class="container-fluid nav-cabeca">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="login.jsp">Lyncas Cadastro</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<%//verifica se a sessão do usuario é válida
						if (session.getAttribute("user") != null) {%>
					<ul class="dropdown-menu">
						<li><a href="favoriteBooks.jsp"><span class="glyphicon glyphicon"></span>Favoritos</a></li>
					</ul>
					<%}%>
				</ul>
				<ul id="Logout" class="nav navbar-nav navbar-right">
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
		<!--/.container-fluid -->
	</nav>
	<main class="container">