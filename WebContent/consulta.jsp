<%@page import="entity.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="js/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<%@include file="/cabecalho.jsp"%>

<%
	ArrayList<Usuario> usuarios = (ArrayList<Usuario>) request.getAttribute("usuarios");
%>

<html>
<head>
<title>Consulta=</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<table border="1">
	<% if (!usuarios.isEmpty()) { %>
				<tr>
				<th>Código</th>
				<th>Nome</th>
				<th>CPF</th>
				<th>Data Nascimento</th>
				<th>Sexo</th>
				<th>Nacionalidade</th>
				<th>Naturalidade</th>
				<th>Data Cadastro</th>
			</tr>
		<%
			for (Usuario usu : usuarios) { %>
				<tr>
					<td><%= usu.getId() %></td>
					<td><%= usu.getNome() %></td>
					<td><%= usu.getCpf() %></td>
					<td><%= usu.getDataNascimento() %></td>
					<td><%= usu.getSexo() %></td>
					<td><%= usu.getNacionalidade() %></td>
					<td><%= usu.getNaturalidade() %></td>
					<td><%= usu.getDataCadastro() %></td>
				</tr>
			<% }
		} else { %>
			<tr>
				<td>Não há informações!</td>
			</tr>
		<%
		} %>
	</table>
	<!-- <form action="consultaServlet" method="get" role="form">
		<button type="submit" id="consultar" class="btn btn-primary btn-lg">Consultar</button>
	</form> -->
	<a class="navbar-brand" href="cadastro.jsp">Ir para Cadastro</a>
	<a class="navbar-brand" href="atualizarDados.jsp">Ir para Atualização</a>
</body>
</html>
