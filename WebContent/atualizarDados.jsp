<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<%@include file="/cabecalho.jsp"%>

<html>
<head>
<title>Atualizar dados</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<form action="atualizarUsuarioServlet" method="post" role="form">
		<div class="input-group col-sm-4">
			<label>Informe o CPF do usuário que será alterado</label>
			<input type="text" id="cpf" class="form-control" placeholder="CPF"
				aria-label="CPF" aria-describedby="basic-addon1" required name="cpf"
				onkeypress="$(this).mask('000.000.000-00');">
		</div>
		<div class="input-group col-sm-4">
			<input type="text" id="nome" name="nome" class="form-control" placeholder="Nome"
				aria-label="Nome" aria-describedby="basic-addon1" required>
		</div>
		<div class="input-group col-sm-4">
			<input type="email" id="email" class="form-control" placeholder="E-mail"
				aria-label="E-mail" aria-describedby="basic-addon1" name="email"
				data-error="E-mail inválido!">
		</div>
		<div class="input-group col-sm-4">
			<input type="date" id="dataNascimento" class="form-control" placeholder="Data de Nascimento"
				aria-label="Data de Nascimento" aria-describedby="basic-addon1" name="dataNascimento"
				 onkeypress="$(this).mask('00/00/0000')" required>
		</div>
		<div class="input-group col-sm-4">
	      <select id="inputSexo" class="form-control" name="sexo">
	        <option selected>Selecione o sexo</option>
	        <option>Masculino</option>
	        <option>Feminino</option>
	        <option>Não Informado</option>
	      </select>
	    </div>
		<div class="input-group col-sm-4">
			<input type="text" id="nacionalidade" class="form-control" placeholder="Nacionalidade"
				aria-label="nacionalidade" aria-describedby="basic-addon1" name="nacionalidade">
		</div>
		<div class="input-group col-sm-4">
			<input type="text" id="naturalidade" class="form-control" placeholder="Naturalidade"
				aria-label="naturalidade" aria-describedby="basic-addon1" name="naturalidade">
		</div>
		<br>
		<button type="submit" id="atualizar" class="btn btn-primary btn-lg">Atualizar</button>
	</form>
	<a class="navbar-brand" href="cadastro.jsp">Ir para Cadastro</a>
</body>

</html>