package servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import conexao.Conexao;

@WebServlet("/atualizarUsuarioServlet")
public class atualizarUsuarioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public atualizarUsuarioServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		String cpfRecebido = request.getParameter("cpf");
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		String dataNascimento = request.getParameter("dataNascimento");
		String sexo = request.getParameter("sexo");
		String nacionalidade = request.getParameter("nacionalidade");
		String naturalidade = request.getParameter("naturalidade");
		
		try {
			Connection con = Conexao.getConnection();
			
			String sql = "update cadastro "
					+ "set nome = '"+nome+"', "
					+ "email = '"+email+"', "
					+ "dataNascimento = '"+dataNascimento+"', "
					+ "sexo = '"+sexo+"', "
					+ "nacionalidade = '"+nacionalidade+"', "
					+ "naturalidade = '"+naturalidade+"', "
					+ "dataCadastro = now() "
					+ "where cpf = '" + cpfRecebido +"'";
			
			java.sql.PreparedStatement sqlPrep = Conexao.getConnection().prepareStatement(sql);
			sqlPrep.execute();

			sqlPrep.close();
			con.close();
			
			request.getRequestDispatcher("/atualizarDados.jsp").forward(request, response);
			
		} catch (Exception e) {
			System.out.println("N�o atualizou os dados! " + e);
		}
		
		
	}

}
