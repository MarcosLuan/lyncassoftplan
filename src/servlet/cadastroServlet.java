package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import conexao.Conexao;

@WebServlet("/cadastroServlet")
public class cadastroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public cadastroServlet() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		String cpf = request.getParameter("cpf");
		String dataNascimento = request.getParameter("dataNascimento");
		String sexo = request.getParameter("sexo");
		String nacionalidade = request.getParameter("nacionalidade");
		String naturalidade = request.getParameter("naturalidade");
		String senha = request.getParameter("senha");
		
		try {
			Connection con = Conexao.getConnection();
			
			String sql = "insert into cadastro(nome,email,cpf,dataNascimento,sexo,nacionalidade,naturalidade,senha,dataCadastro)"
					+ " value(?,?,?,?,?,?,?,?,now())";
			
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, nome);
			stmt.setString(2, email);
			stmt.setString(3, cpf);
			stmt.setString(4, dataNascimento);
			stmt.setString(5, sexo);
			stmt.setString(6, nacionalidade);
			stmt.setString(7, naturalidade);
			stmt.setString(8, senha);
			
			stmt.execute();
			stmt.close();
			con.close();
			
			request.getRequestDispatcher("/cadastro.jsp").forward(request, response);
			
		} catch (Exception e) {
			System.out.println("N�o enviou os dados! " + e);
		}
		
	}

}


	
