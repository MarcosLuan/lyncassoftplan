package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import conexao.Conexao;
import entity.Usuario;

@WebServlet("/consultaServlet")
public class consultaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public consultaServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		try {
			String sql = "select * from cadastro";
			java.sql.PreparedStatement sqlPrep = Conexao.getConnection().prepareStatement(sql);
			ResultSet rs = sqlPrep.executeQuery();
			List<Usuario> usuarios = new ArrayList<>();

			while (rs.next()) {
				Usuario usuario = new Usuario();
				
				usuario.setId(rs.getInt("idCadastro"));
				usuario.setNome(rs.getString("nome"));
				usuario.setEmail(rs.getString("email"));
				usuario.setCpf(rs.getString("cpf"));
				usuario.setDataNascimento(rs.getString("dataNascimento"));
				usuario.setSexo(rs.getString("sexo"));
				usuario.setNacionalidade(rs.getString("nacionalidade"));
				usuario.setNaturalidade(rs.getString("naturalidade"));
				usuario.setSenha(rs.getString("senha"));
				usuario.setDataCadastro(rs.getString("dataCadastro"));
				usuarios.add(usuario);
			}
						
//			request.getRequestDispatcher("/consulta.jsp").forward(request, response);
			request.setAttribute("usuarios", usuarios);
			String pagina = "/consulta.jsp";
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
			dispatcher.forward(request, response);
			
		} catch (Exception e) {
			System.out.println("N�o recebeu os dados! " + e);
		}
	}
}
